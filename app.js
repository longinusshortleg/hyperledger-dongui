var express = require('express')
  , http = require('http')
  , path = require('path')
  , app = express()
  , fs = require('fs')
  , url = require('url')
  , bodyParser = require('body-parser')
  , logger = require('morgan');

app.use(express.static(path.join(__dirname, 'public')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

var server = app.listen(3000, function () {
   var host = server.address().address;
   var port = server.address().port;
   console.log("Example app listening at http://%s:%s", host, port);
});

app.get('/', function (req, res) {
	var viewPath = __dirname + '/index.html';
	fs.readFile(viewPath, function(error, data){
		if (error){	
			console.log(viewPath);
			console.log(error);
			res.writeHead(404);
			res.write("opps this doesn't exist - 404");
			res.end();
		}
		else{
			res.writeHead(200, {"Content-Type": "text/html"});
			res.write(data, "utf8");
			res.end();
		}
	});
});

function getPeerNumber(peer){
	switch(peer) {
		case "ANZ":
			return 0;
		case "ASLL":	
			return 1;
		case "BEND":
			return 2;
		case "CBAA":
			return 3;
	    	default:
			break;
	} 
}

const execSync = require('child_process').execSync;
app.post('/getall', function(req, res) {
	var myCommand = 'node ../fabric-samples/fabcar/query_test_lionel.js ' + getPeerNumber(req.body.peer) + ' queryAllUsers';
	code = execSync(myCommand);
	var massagedResult = code.toString().split("ResponseMarker")[1];
	var ledgers = JSON.parse('['+massagedResult+']');
	res.json({result: 'success', ledgers: ledgers});
});


app.post('/add', function(req, res) {
	var myCommand = 'node ../fabric-samples/fabcar/invoke_test_lionel.js createUser ' + req.body.icno + ' \''
			+ req.body.custname + '\' ' + req.body.peer  + ' ' + req.body.accountno + ' ' + req.body.email;
	console.log(myCommand);
	code = execSync(myCommand);
	var codeStr = code.toString();
	console.log('-----------1');
	console.log(codeStr);
	console.log('-----------2');
	console.log(codeStr.indexOf('The transaction has been committed on peer'));
	if (codeStr.indexOf('The transaction has been committed on peer') > -1) {
		res.json({result: 'success', message:code.toString()});
	}
	else {
		res.json({result: 'failed', message:'Endorsement policy failure'});
		
	}
});
//app.post('/update', routes.update);
//app.post('/delete', routes.delete);oStr
